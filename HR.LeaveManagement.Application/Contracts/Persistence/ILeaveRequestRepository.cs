﻿using HR.LeaveManagement.Domain;

namespace HR.LeaveManagement.Application.Contracts.Persistence
{
    public interface ILeaveRequestRepository : IGenericRepository<LeaveRequest>
    {
        Task<LeaveRequest> GetById(int id);

        Task<List<LeaveRequest>> GetAll();

        Task<List<LeaveRequest>> GetAllByUserId(string userid);
    }
}
