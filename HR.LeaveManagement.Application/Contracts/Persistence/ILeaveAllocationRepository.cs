﻿using HR.LeaveManagement.Domain;

namespace HR.LeaveManagement.Application.Contracts.Persistence
{
    public interface ILeaveAllocationRepository : IGenericRepository<LeaveAllocation>
    {
        Task<LeaveAllocation> GetLeaveAllocationWithDetails(int id);

        Task<List<LeaveAllocation>> GetLeaveAllocationWithDetails();

        Task<List<LeaveAllocation>> GetLeaveAllocationsWithDetails(string userId);

        Task<bool> AllocationExist(string userId, int LeaveTypeId, int period);

        Task AddAllocation(List<LeaveAllocation> allocation);

        Task<LeaveAllocation> GetUserAllocations(string userId, int LeaveTypeId);
    }
}
