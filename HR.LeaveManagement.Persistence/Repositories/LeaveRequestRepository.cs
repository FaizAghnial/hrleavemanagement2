﻿using HR.LeaveManagement.Application.Contracts.Persistence;
using HR.LeaveManagement.Domain;
using HR.LeaveManagement.Persistence.DatabaseContext;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.LeaveManagement.Persistence.Repositories
{
    public class LeaveRequestRepository : GenericRepository<LeaveRequest>, ILeaveRequestRepository
    {
        public LeaveRequestRepository(HrDatabaseContext context) : base(context) { 
        }
        
        public async Task<List<LeaveRequest>> GetAll()
        {
            return await _context.LeaveRequests.ToListAsync();
        }

        public async Task<List<LeaveRequest>> GetAllByUserId(string userid)
        {
            return await _context.LeaveRequests
                .Where(q => q.RequestingEmployeeId == userid)
                .Include(q => q.LeaveType)
                .ToListAsync();
        }

        public async Task<LeaveRequest> GetById(int id)
        {
            return await _context.LeaveRequests
                .FirstOrDefaultAsync(q => q.Id == id);
        }
    }
    
}
